# qt5_ex.py

import sys
from PyQt5.QtWidgets import (
        QApplication, QWidget, QLabel, QPushButton, QComboBox, QFrame, QMainWindow,
        QMessageBox

    )
from PyQt5.QtCore import pyqtSlot, QRect, Qt

class MainPage(QMainWindow):
    def __init__(self, title=" "):
        super().__init__()  # inherit init of QWidget
        self.title = title
        self.left = 250
        self.top = 250
        self.width = 400
        self.height = 300
        self.widget()

    def widget(self):
        # window setup
        self.setWindowTitle(self.title)
        # self.setGeometry(self.left, self.top, self.width, self.height)
        ## use above line or below
        # self.resize(self.width, self.height) # resizable
        self.setFixedSize(self.width, self.height)  # fixed size
        self.move(self.left, self.top)

        self.menubar= self.menuBar() # add menu bar
        self.helpMenu = self.menubar.addMenu("&Help")  # Add Help in menu bar
        self.about = self.helpMenu.addAction("&About")  # Add option in Help
        self.about.setShortcut("F11") # display F11 as shortcut 
        self.about.triggered.connect(self.aboutDef)

        self.credits = self.helpMenu.addAction("&Credits") # Add another option in Help
        
        self.messageBox = QMessageBox(self)
        self.messageBox.setFixedSize(self.width, self.height)  # fixed size

        self.show()

    @pyqtSlot()
    def aboutDef(self):
        message = "Parameters:\n  i = integer  d = double" 
        self.messageBox.about(self, "Help ", message)
        # self.messageBox.question(self, "Parameter help", message, QMessageBox.Ok)
        
def main():
    app = QApplication(sys.argv)
    w = MainPage(title="PyQt5")
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
